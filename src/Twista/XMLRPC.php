<?php

/**
 * XMLRPC abstraction
 * uses curl extension and xmlrpc extension
 * @author Michal Haták <me@twista.cz>
 */
namespace Twista;


class XMLRPC {

    /** @var null|string url */
    protected $url = null;

    /**
     * @param string $url server url
     */
    public function __construct($url) {
        $this->url = $url;
    }

    /**
     * send xmlrpc request
     * @param string $method
     * @param array $params
     * @return array
     */
    public function send($method, $params) {
        $request = \xmlrpc_encode_request($method, $params, array('encoding' => 'utf-8'));
        $handle = curl_init();

        curl_setopt($handle, CURLOPT_URL, $this->url);

        curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($handle, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($handle, CURLOPT_HTTPHEADER, array("Content-Type: text/xml;charset=utf-8", "Content-Length: " . mb_strlen($request)));
        curl_setopt($handle, CURLOPT_POSTFIELDS, $request);

        $response = curl_exec($handle);

        curl_close($handle);

        return xmlrpc_decode($response, 'utf-8');
    }

    /**
     * encode given value via xmlrpc
     * @param mixed $data
     * @return string
     */
    public function encode($data) {
        return \xmlrpc_encode($data);
    }
}