<?php
/**
 * sklik keyword object
 * @author Michal Haták <me@twista.cz>
 */
namespace Twista\Sklik;

class Keyword extends Object {

    /** @var  string */
    protected $name;

    /** @var  string ['broad','phrase','exact','negativeBroad','negativePhrase','negativeExact'] */
    protected $matchType;

    /** @var  int */
    protected $cpc;

    /** @var  string */
    protected $url;

}