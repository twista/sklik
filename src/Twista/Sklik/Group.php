<?php
/**
 * sklik group object
 * @author Michal Haták <me@twista.cz>
 */
namespace Twista\Sklik;

class Group extends Object {

    /** @var  string */
    protected $name;

    /** @var  int */
    protected $cpc;

    /** @var  int */
    protected $cpcContext;

}


