<?php
/**
 * slik pattern object
 * @author Michal Haták <me@twista.cz>
 */
namespace Twista\Sklik;

class Pattern extends Object {

    /** @var  string */
    protected $patern;

    /** @var  int */
    protected $cpc;

}