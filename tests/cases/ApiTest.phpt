<?php

require __DIR__ . '/../bootstrap.php';

class ApiTest extends Tester\TestCase {

    /** @var  \Twista\Sklik\Api */
    protected $api;

    public function setUp() {
        global $config;
        \Tester\Assert::equal(include(__DIR__ . '/../config.test.php'), $config);
        $this->api = new \Twista\Sklik\Api($config);
    }

    /**
     * prepare test environment
     * + some test
     */
    public function testRemoveAll() {
        $campaigns = $this->api->getCampaigns();
        foreach ($campaigns as $campaign) {
            $id = $campaign->id;
            if ($campaign->removed == true) {
                \Tester\Assert::true($this->api->restore($campaign));
                $temp = $this->api->getCampaigns($id);
                \Tester\Assert::false($temp->removed);
            }

            \Tester\Assert::true($this->api->delete($campaign));
            $c = $this->api->getCampaigns($id);
            \Tester\Assert::true($c->removed);
        }
    }

    /**
     * this test isn't necessary, but surely says if method calling works properly
     */
    public function testSearchServicesAndRegions() {
        $services = json_decode('[{"id":1,"name":"Vyhled\u00e1v\u00e1n\u00ed na Seznam.cz"},{"id":2,"name":"Firmy.cz"},{"id":3,"name":"Sbazar.cz"},{"id":4,"name":"Encyklopedie.Seznam.cz"},{"id":5,"name":"Seznam na mobil (Smobil.cz)"},{"id":6,"name":"Seznam Obr\u00e1zky (Obrazky.cz)"},{"id":7,"name":"Seznam Zbo\u017e\u00ed (Zbozi.cz)"},{"id":8,"name":"Partnersk\u00e9 vyhled\u00e1va\u010de"}]', true);
        \Tester\Assert::same($this->api->getSearchServices(), $services);

        $regions = json_decode('[{"id":10001,"name":"Jiho\u010desk\u00fd"},{"id":10002,"name":"Plze\u0148sk\u00fd"},{"id":10003,"name":"Karlovarsk\u00fd"},{"id":10004,"name":"\u00dasteck\u00fd"},{"id":10005,"name":"Libereck\u00fd"},{"id":10006,"name":"Kr\u00e1lov\u00e9hradeck\u00fd"},{"id":10007,"name":"Pardubick\u00fd"},{"id":10008,"name":"Olomouck\u00fd"},{"id":10009,"name":"Zl\u00ednsk\u00fd"},{"id":10010,"name":"Hlavn\u00ed m\u011bsto Praha"},{"id":10011,"name":"St\u0159edo\u010desk\u00fd"},{"id":10012,"name":"Moravskoslezsk\u00fd"},{"id":10013,"name":"Vyso\u010dina"},{"id":10014,"name":"Jihomoravsk\u00fd"}]', true);
        \Tester\Assert::same($this->api->getRegions(), $regions);
    }

    public function testAddAndRemoveCampaigns() {

        $api = $this->api;
        \Tester\Assert::exception(function () use ($api) {
            $campaign = new Twista\Sklik\Campaign(array());
            $this->api->check($campaign);

        }, 'InvalidArgumentException');

        $campaign = new Twista\Sklik\Campaign(array(
            'name' => 'Test campaign name' . time(),
            'dayBudget' => 10000,
            'excludedSearchServices' => array(
                1
            ),
            'excludedUrls' => array('http://www.twista.cz'),
            'totalBudget' => 10000,
            'totalClicks' => 10000));

        $campaign_count = count($this->api->getCampaigns());

        \Tester\Assert::true($this->api->check($campaign));
        \Tester\Assert::true($this->api->create($campaign));

        \Tester\Assert::equal($campaign_count + 1, count($this->api->getCampaigns()));


    }

    public function tearDown() {

    }

}

\run(new ApiTest());
